<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "wзззeb" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web']],function(){

     Route::get('laravel.ua/{slug}', ['as' => 'sitepage.single', 'uses' =>'BlogController@getSingle' ])->where('slug', '[\w\d\-\_]+');

     Route::get('laravel.ua', ['uses' => 'BlogController@getIndex', 'as' => 'sitepage.index' ]);

     Route::get('blog', ['users' => 'BlogController@getIndex', 'as' => 'blog.index']);

     
     


     
      Route::get('contact',  'PagesController@getContact');


      Route::post('contact',  'PagesController@postContact');
     
     Route::get('about',  'PagesController@getAbout'); 

     Route::get('/', 'PagesController@getIndex');

     Route::resource('posts', 'PostController');



     
});







Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

