@extends('main')
 
 @section('title', '| About')

  @section('content')
  <style type="text/css">
  	.col-md-6{
  		margin:0px 0px !important;
  		padding: 0px 0px!important;
  		
  	}
  	img{
  		padding-left: 65px !important;
  		border-radius: 0px !important;
  		margin-top: 26px !important;
  	}
  	
  	
  </style>
   <div class="container">
        <div class="row">
             <div class="col-md-12"><h1>To your submission</h1></div>
        </div>
   <a href="http://www.suffolkliner.com/"> <img  style="opacity:1;" src="../images/fon3.jpg" class="img-rounded"></a>
   <a href="https://www.planetfitness.com/"> <img  style="opacity:1;" src="../images/fon2.jpg" class="img-rounded"></a>
   <a href="http://www.tfusa.net/"><img  style="opacity:1;" src="../images/fon4.jpg" class="img-rounded"></a>
   <a href="https://primefitnessusa.com/"> <img  style="opacity:1;" src="../images/fon5.jpg" class="img-rounded"></a>
        <div class="row" >
        	 <div class="col-md-12">
        	 	  
            		                      	<h2>Training Tips.</h2>
 <p>
 	Ask almost any personal trainer and they’ll tell you that regardless of your training goals, healthy eating is the backbone. Food is what fuels your body to reach your goals, and without proper nutrition through quality foods, you’re likely to stall. Maintain a balanced diet consisting of fruits, vegetables, complex carbohydrates, complete proteins, and healthy fats like fish oils and flaxseeds.
 	Preparing meals in advance gives you the best chance to accomplish your nutrition goals, says Micah LaCerte, a personal trainer and fitness competition world champion. That way, he says, you won’t feel pressured to eat unhealthy foods or skip meals. Check out 10 of our favorite simple meal-prep recipes.
 	Eating only three daily meals? Not a great idea. “Half the people I deal with aren’t losing weight because they don’t eat enough,” says veteran personal trainer Mike Duffy. Duffy advises his clients “to eat five times a day, about every three hours, to stimulate their metabolism” including two mini-meals between three basic meals. With activity levels decreasing throughout the day, he advises to “eat less as the day goes on.”
 	You’ll be eating more often, so paying attention to portions is extremely important. “Make sure chicken breasts, (and) meats, are no larger than your palm, and that pastas are no larger than your fists,” says Jay Cardiello, a personal trainer to countless celebrities and professional athletes. He also suggests using “smaller bowls, plates, and cups” because studies show people “serve themselves 20-40% more food when they’re using larger plates.” Here’s how to estimate portion sizes.
 	Everything you consume should have substantial nutritional value. “You want the most nutritional bang for your buck,” says Dan Trink, C.S.C.S., a strength coach and trainer. “Everything you eat should serve some sort of nutritional purpose in your body, fuel your workouts, and (be) geared toward optimizing your body.”
 	Talk to any personal trainer and they’ll tell you there are certain muscle-building basics. First, increase your caloric and complete protein intake, so your body has enough building blocks to get bigger. Then, when you enter the gym, focus on your form. Perform compound movements and train with weights on average around four times a week. Never underestimate the importance of rest. Remember, muscle tissue grows outside of the gym when you’re giving your body time to relax and recover following your workouts.
 	Don’t take any shortcuts. “Aim for the largest range of motion you can achieve in your exercises,” says Lee Boyce, C.P.T. “Your muscles will do more work per rep, and it will result in your breaking down more tissue by the end of the workout.”
 	Wondering how to get the most out of lifting weights? “Use a weight that will have you failing on the set between the 30- and 40-second mark,” Duffy says. Time under tension causes muscle to grow. “If you’re failing at 20 seconds, you know that weight was too heavy.”
 	If getting huge is your goal, then throttle back on your cardio workouts, says LaCerte—chances are, you’ll be burning far too many calories. So what should you do if you still want to get in some cardio? LaCerte says “a light jog a few days per week for 20 minutes is adequate.” If you’re aiming to burn fat, of course, then focus on getting enough protein every day (usually one gram of protein per pound of ideal body weight), while still keeping your overall caloric intake low.
 	Some trainers and lifters feel supplements can play a key role in boosting muscle gains. If you subscribe to that theory, then chances are, you’re already taking protein supplements—but what else? Creatine, for one, “seems to be about the most effective strength- and size-building supplement,” Trink says. To boost your performance, you may also want to try peppermint. Cardiello explains that the scent “alters the perception of how hard you’re working out,” making it seem “less strenuous, slower-paced, and easier to complete.”
 	
 </p>
        	 </div>
        </div>
    
</div>
    



  @endsection





 <!--

        <div class="row">
        	 <div class="col-md-6">
       	 	      <img  style="opacity:1;" src="../images/fon3.jpg" class="img-responsive"><b>Poll</b>
       	 	      <img  style="opacity:1;" src="../images/fon2.jpg" class="img-responsive"><b>Training center</b>
       	 	      <img  style="opacity:1;" src="../images/fon4.jpg" class="img-responsive"><b>Training center</b>


        	 </div>
        	 <div class="col-md-6">
        	 	  	<p>
            		                      	<h2>Training Tips.</h2>
      1. Drink plenty of water. And during training.<br>
      2. Practice at a slow pace. Raise at the expense of 3 (4) and omit at the expense of 3 (4).<br>
      3. Do not accept all the advice that you will be given. To the beginner, everyone is ready to give advice. And sometimes these tips will be contradictory to each other. And often even the instructors of the hall give opposing advice. To avoid porridge in your head, select one of the most experienced person (me, for example), and listen to his opinion. And all the rest of the advice at first (up to six months of training) should be ignored.<br>
       4.Do not exercise if your muscles hurt a lot from your previous workout. When the muscles slightly ache, you can hold a lesson. But, if you engage through severe pain, the muscle fiber can not stand it and this often leads to injury.<br>
      5. Engage only in the complex. Make it right for your abilities. Do not exercise just like that. Moving from simulator to simulator.<br>
      6. Changes in the shape occur no earlier than after three months of proper training. Do not wait for results every day and do not get up every day on the scales. Scales are a bad helper in determining the results of training. Determine the result of the photographs. Take a photo every three months.<br>
            	</p>
        	 </div>
        	 	
        	
        </div>









































 <div class="row">
        	<div class="col-md-6"><img  style="opacity:1;" src="../images/fon2.jpg" class="img-responsive"><b>Training center</b></div>
                 <div class="col-md-1"></div>

            <div class="col-md-5">
            	<p>
            		                      	<h2>Training Tips.</h2>
      1. Drink plenty of water. And during training.<br>
      2. Practice at a slow pace. Raise at the expense of 3 (4) and omit at the expense of 3 (4).<br>
      3. Do not accept all the advice that you will be given. To the beginner, everyone is ready to give advice. And sometimes these tips will be contradictory to each other. And often even the instructors of the hall give opposing advice. To avoid porridge in your head, select one of the most experienced person (me, for example), and listen to his opinion. And all the rest of the advice at first (up to six months of training) should be ignored.<br>
            	</p>

            </div>
        </div>	
        <div class="row">
        	<div class="col-md-4"></div>
        	    <div class="col-md-4"><img  style="opacity:1;" src="../images/fon3.jpg" class="img-responsive"><b>Poll</b>
        	    </div>
        	    <div class="col-md-4"></div>
        </div>
             <div class="row">
        	        <div class="col-md-4"></div>
        	    <div class="col-md-4">
        	    </div>
        	    <div class="col-md-4"><img style="opacity:1;" src="../images/fon4.jpg" class="img-responsive"><b>Training center</b></div>
            </div>
            <div class="row">
            	<div class="col-md-4"></div>
            	<div class="col-md-4">
                      <p>
      4.Do not exercise if your muscles hurt a lot from your previous workout. When the muscles slightly ache, you can hold a lesson. But, if you engage through severe pain, the muscle fiber can not stand it and this often leads to injury.<br>
      5. Engage only in the complex. Make it right for your abilities. Do not exercise just like that. Moving from simulator to simulator.<br>
      6. Changes in the shape occur no earlier than after three months of proper training. Do not wait for results every day and do not get up every day on the scales. Scales are a bad helper in determining the results of training. Determine the result of the photographs. Take a photo every three months.<br>


                      </p>
                </div>
            	<div class="col-md-4"></div>
            </div>
