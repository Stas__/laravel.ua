<style type="text/css">

  label{
  font-size: 18px; 
  color: #585858; 
  letter-spacing: 3px;
      }
      .container{
        width: 100% !important;
        padding:2 !important; 
      }
      form{
        padding: 50px 50px;
      }
      .fon{
        background-image: linear-gradient(#E6E6E6, #FAFAFA, #E6E6E6);
        height: 100px;
        margin-bottom: 0px;
        
      }
</style>

@extends('main')

@section('title', '| Contact')

@section('content')
    

      <div class="container" style="height: 665px !important;">
        <div class="row">
            <div class="col-md-12">
                  <h1 class="fon" style="padding-top:31px; "> &#8195;To contact us</h1>
                    <hr>
                    <br>
                    <form action ="{{ url('contact') }}" method ="POST">
                      {{ csrf_field() }}
                      <div class="form-group">
                          <label name="email">Email:</label>
                          <input id="email" name="email" class="form-control">
                      </div><br>
                      <div class="form-group">
                          <label name="subject">Subject:</label>
                          <input id="subject" name="subject" class="form-control">
                      </div>
                      <div class="form-group">
                          <label name="message">Message:</label>
                          <textarea id="message" name="message" class="form-control">Type your message here...</textarea>
                      </div><br>
                   <div class="text-right">  <input type="submit" value="Send Message" class="btn btn-success btn btn-md" />
                    </div>
                    </form>
            </div>
        </div>
@endsection
































   
           



















