
@extends('main')
 @section('title', '| Homepage')
    @section('content')
        <style type="text/css">
           .col-offset-1{
            line-height: 50px;
            width: 10px;
                      }
            button{
              height: 30px !important;
              width: 210px  !important;
            } 
            
                    
        </style>
        <div class="row">
             <div class="col-md-12">
                  <div class="block"><img src="../images/foto1.jpeg" class="img-responsive"></div>
             </div>
        </div>
       <div class="row">
            <div class="col-md-12">
                <div class="jumbotron" style="color: #202020; background: #E1E1E1;" >
                     <h1>Sport as a way of life</h1>
                     <p>The following are articles about the blog.</p>
                     <p><a class="btn btn-primary btn-lg" href="http://127.0.0.1:8000/posts/create" role="button" style="color: white !important;">Bring in Post<a></p>
                </div>
            </div>
        </div>
              <div class="row">
                  <div class="col-md-8">
                       <br>
                       <div class="post">
                            <h3 style="line-height: 1.3; font-weight: bold;  letter-spacing: 4px;">Proper nutrition</h3><br>
                                <img  style="opacity:1;" src="../images/im1.jpeg" class="img-responsive">
                                <br><p>When you exercise hard for 90 minutes or more, especially if you're doing something at high intensity that takes a lot of endurance, you need a diet that can help you perform at your peak and recover quickly afterward.</p>
                                <a href="https://www.webmd.com/fitness-exercise/features/nutrition-tips-athletes#1" class="btn btn-warning" >Read More</a>
                       </div>
                         <br>
                       <div class="post">
                            <h3 style="line-height: 1.3; font-weight: bold;  letter-spacing: 4px;">Sport life as the key <br >To discovering health</h3><br>
                            <img style="opacity:1;" src="../images/im2.jpg" class="img-responsive"><br>
                                <p>Sport is probably as old as the humanity itself. It has been developing with the developing and growth of the mankind. To my mind we can hardly overestimate the meaning of sport in our life and day-to-day activities, because its main purpose is to bring up the harmoniously developed generation - the generation of strong and healthy people. Sport makes our bodies strong, quickens our reaction, and shapes the wits.</p>
                                <a href="https://www.englishtopics.net/topicsmenu/4-topicshigh/8-sport-in-my-life" class="btn btn-primary">Read More</a>
                       </div>
                       <br>
                       <div class="post">
                            <h3 style="line-height: 1.3; font-weight: bold; letter-spacing: 4px;">Sports Equipment </h3><br>
                            <img style="opacity:1;"  src="../images/im3.jpg" class="img-responsive"><br>    
                                <p>Articles used in physical training and sports, manufactured in conformity with standards approved by international or national (in the case of national sports) sports federations and registered with international or state organizations governing standards, for example, Gosstandart (State Standard Administration of the USSR). Sports equipment is divided into the following principal groups: clothing (uniforms), footwear, apparatus, and equipment for sports structures and facilities and for officiating.</p>
                                <a href="https://encyclopedia2.thefreedictionary.com/Sports+Equipment" class="btn" style="background: #A4A4A4; color: #2E2E2E;">Read More</a>
                       </div>
                         
                 </div>
                  <div class="col-md-3 col-offset-1" style="margin-left:39px; ">
                        <h2 style="font-size: 36px;">Sports facilities</h2>
                        <img style="width:210px; opacity:1;"  src="../images/im4.jpg" class="img-rounded">
                        <a href="https://www.fitnessmagazine.com/workout/tips/the-best-gyms-in-america/"><button type="button" class="btn btn-primary btn-sm">Fitnes club</button></a><br>
                        
                        <img style=" width:210px; opacity:1; margin-top: 50px;"  src="../images/im5.jpg" class="img-rounded">
                        <a href="https://www.equinox.com/"><button type="button" class="btn btn-success btn-sm" >Fitnes club</button></a><br>
                       
                  </div>


              </div>
              
              <br>
              <h1 style="font-size: 36px; font-weight: bold; text-align: center;">Board reviews</h1>
              <br>
               @foreach($posts as $post) 
                       <div class="post">
                            <h3>{{ $post ->title }} </h3>
                            <p>{{ substr($post->body, 0,300) }}</p>
                            <a href="{{ url('laravel.ua/'. $post->slug) }}" class="btn btn-primary">Read More</a>
                       </div>
                  @endforeach
 
      @endsection





















































    

    