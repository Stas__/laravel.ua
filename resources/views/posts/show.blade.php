@extends('main')

@section('title', '| View Post')

@section('content')
<style type="text/css">
    .container{
        height: 665px !important;
        
    }
</style>

<div class="row">
     <div class="col-md-8">
         <h1 style="font-size:42px; color: #2E2E2E; 
         letter-spacing: 4px;">{{ $post->title }}</h1>

         <p class="lead" style="font-size:26px; color: #585858; letter-spacing: 3px;">{{ $post->body }}</p>
     </div>
     <div class="col-md-4">
     	 <div class="well">
             <dl class="dl-horizontal">
                  <label>Url:</label>
                  <p><u><a href="{{ route('sitepage.single', $post->slug) }}" style="color: #848484 !important;"> {{ route('sitepage.single', $post->slug) }}</a></u></p>
              </dl>
     	 	  
              <dl class="dl-horizontal">
     	 	  	  <label>Create At:</label>
     	 	  	  <p style="color: #6E6E6E">{{ date('M j, Y H:i', strtotime ($post->created_at)) }}</p>
     	 	  </dl>

     	 	  <dl class="dl-horizontal">
     	 	  	  <label>Last Updated:</label>
     	 	  	  <p style="color: #6E6E6E">{{ date('M j, Y H:i', strtotime ($post->updated_at)) }}</p>
     	 	  </dl>
             <hr>
     	       <div class="row">
     	         <div class="col-sm-4"><a href="http://127.0.0.1:8000/posts/"><button type="button" class="btn  btn-block" style="background: #939393; color: white;">&laquo;&nbsp;Posts</button></a></div>

                 <div class="col-sm-4" >

     	         	{!! Html::LinkRoute('posts.edit', 'Edit', array($post->id), 

     	         	array('class' => 'btn btn-warning btn-block ')) !!}
     	         	
     	         	 <!--<a href="#" class="btn btn-block" style="background: #AEB404; color: #1C1C1C;">Edit</a><br>-->
     	         	
     	         </div>
     	         <div class="col-sm-4">
     	         	 
                     {!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE']) !!}

                     {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

                    {!! Form::close() !!}
     	         	
     	         </div>
               </div>

     	 </div>
    </div>
</div>

@endsection
