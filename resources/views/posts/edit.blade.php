@extends('main')

@section( 'title', '| Edit Blog Post')

@section('content')

<style type="text/css">
  .container{
    height: 665px !important;
}
</style>

<div class="row">
   {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT' ]) !!}
     <div class="col-md-8">
         
           {{ Form::label('title', 'Title:') }}

           {{ Form:: text('title', null, ["class"=> 'form-control input-lg']) }}

           {{  Form::label('slug', 'Slug' , ['class' => 

           'form-spacing-top']) }}
           
           {{ Form::text('slug', null, ['class' => 'form-control']) }}


           {{ Form::label('body', "Body:", ['class'=> 'form-spacing-top']) }}


           {{ Form::textarea('body', null, ['class' => 'form-control']) }}


     </div>
     <div class="col-md-4">
     	 <div class="well">
     	 	  <dl class="dl-horizontal">
     	 	  	  <dt>Created At:</dt>
     	 	  	  <dd style="color: #6E6E6E">{{ date('M j, Y H:i', strtotime ($post->created_at)) }}</dd>
     	 	  </dl>

     	 	  <dl class="dl-horizontal">
     	 	  	  <dt>Last Updated:</dt>
     	 	  	  <dd style="color: #6E6E6E">{{ date('M j, Y H:i', strtotime ($post->updated_at)) }}</dd>
     	 	  </dl>
             <hr>
     	       <div class="row">
     	         <div class="col-sm-6">

     	         	{!! Html::LinkRoute('posts.show', 'Cancel', array($post->id), 

     	         	array('class' => 'btn btn-danger btn-block')) !!}
     	         	
     	         	 <!--<a href="#" class="btn btn-block" style="background: #AEB404; color: #1C1C1C;">Edit</a><br>-->
     	         	
     	         </div>
     	         <div class="col-sm-6">
     	         	
     	         	{{ Form::submit('Save Changes', ['class' =>'btn btn-success btn-block']) }}

                     
     	         </div>
               </div>

     	 </div>
     </div>
           {!! Form::close() !!}
</div>
@endsection