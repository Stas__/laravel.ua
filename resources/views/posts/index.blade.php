@extends('main')

@section('title', ' |All Posts')

@section('content')
<style type="text/css">
  
</style>
   <div class="row">
   	   <div class="col-md-10">
   	   	    <h1 style="color: #3C3C3C;">All Posts</h1>
   	   </div>
   	   <div class="col-md-2">
   	   	    <a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing" style="color: white !important;">Create New Post</a>
   	   </div>
   	    
   </div>
   <div class="row">
   	    <div class="col-md-12">
   	    	 <table class="table">
   	    	 	   <thead>
   	    	 	   	      <th>#</th>
   	    	 	   	      <th>Title</th>
   	    	 	   	      <th>Body</th>
   	    	 	   	      <th>Create At</th>
   	    	 	   	      <th></th>
   	    	 	   </thead>
                   <tbody>
                   	   @foreach ($posts as $post)
                           
                           <tr>
                           	   <th>{{ $post->id }} </th>
                           	   <td> {{ $post->title }} </td>
                           	   <td> {{ $post->body }} </td>
                           	   <td> {{ date('M j, Y', strtotime($post->created_at)) }}</td>
                           	   <td> <a href="{{route('posts.show', $post->id)}}" style ="background: #5C8648; " class="btn btn-default btn-sm ">View</a>
                                        
                                <a href="{{ route('posts.edit', $post->id) }}" style="width:47px; margin-top:17px; background: #D3D7D2;" 	class="btn btn-default btn-sm">Edit</a></td>
                           </tr>

                   	   @endforeach
                   </tbody>

   	    	  </table>
                   <div class="text-center">
                     {!! $posts->Links(); !!}
                   </div>
           </div>
    </div>
@stop
