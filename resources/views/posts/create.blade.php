@extends('main')

@section('title', '| Create New Post')



@section('content')
<style type="text/css">
	.container{
		height: 670px;
	}
</style>
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<h1>Create New Post</h1>
		<hr>
		
		  {!! Form::open(array('route' => 'posts.store')) !!}
	        
			        {{ Form::label('title', 'Title:') }}
			        
			        {{ Form::text('title', null, array('class' => 'form-control')) }}
				    
				    <!---->
				   
				   {{ Form::label('slug', 'Slug:')}}

				   {{ Form::text('slug', null, array('class' => 

				   'form-control', 'required' => '', 'minlength' => '5', 

				   'maxlength' => '255')) }}
                    
                     <!---->

		     	    {{Form::label('body', "Post body:") }}

		            {{Form::textarea('body', null, array('class' => 'form-control')) }}

		            {{Form::submit('Create Post',array('class' => 'btn btn-success btn-lg btn-block', 'style' =>'margin-top:20px;')) }}

		  {!! Form::close() !!}
	 <a href="http://127.0.0.1:8000/posts/"> <button type="button" class="btn btn-secondary btn-lg btn-block" style="margin-top: 20px; color: #292929;">Back to all posts</button></a>
	</div>
</div>

@endsection

