@extends('main')

@section('title', '|laravel.ua')

@section('content')
<style type="text/css">
	.container{
		background: #D8D8D8;

	}
</style>
<div class="row">
	 <div class="text-center ">
		  <br><h1>Board</h1><br>
     </div>
</div>

@foreach ($posts as $post)

<div class="row">
	<div class="text-left" style="margin-left: 100px;">
<br>		
		<h2>{{ $post->title }}</h2>

		<h5>Published: {{ date('M j, Y', strtotime($post->created_at )) }} </h5>

		<p>{{ substr($post->body, 0, 250) }} {{ strlen($post->body) >250 ? '...' : ""}}</p>

		<a href=" {{ route('sitepage.single', $post->id ) }}" class="btn btn-success btn-sm" style="color: white !important;">Read more</a>
		<hr>
	</div>
	<div class="col-md-9"></div>
</div>

@endforeach

<div class="row">
	<div class="text-center">
		
		{!! $posts->links() !!}

	</div>
</div>

@endsection