<style type="text/css">
	.container{
		height: 670px !important;
		padding: 0 !important;
	}
</style>


@extends('main')

@section('title', '| $post->title')

@section('content')

<div class="row">
	 <div class="col-md-8 col-md-offset-2">
	 	<h1>{{ $post->title }}</h1>
<h5 style="color: grey; letter-spacing: 3px; margin-left: 12px;">Content</h5>	 	
{{ $post->body }}
	 </div>
</div>
@endsection
