<!DOCTYPE html>
<html lang="en">
<head>
@include('sections page.head')
</head>

  <body>
                   
    @include('sections page.nav')
    <div class="container">
        
        @include('messages')

        @yield('content')
    </div>

    @include('sections page.footer')

    @include('sections page.javascript')
  </body>
</html>