
<nav class="navbar navbar-default">
                <div class="container-fluid" style="background: #E9E9E9;">
                  <!-- Brand и toggle сгруппированы для лучшего отображения на мобильных дисплеях -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><p class="font">Laravel blog</p></a>
                  </div>
           
                  <!-- Соберите навигационные ссылки, формы, и другой контент для переключения -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <li class="{{ Request::is('/') ? "active" : ""}}"><a href="/">Home</a></li>
                      <li class="{{ Request::is('about') ? "active" : ""}}"><a href="/about">About</a></li>
                       <li class="{{ Request::is('laravel.ua') ? "active" : ""}}"><a href="/laravel.ua">Publication</a></li>
                      <li class="{{ Request::is('contact') ? "active" : ""}}"><a href="/contact">Contact</a></li>
                   </ul>
                  </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
              </nav>