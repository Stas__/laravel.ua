<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <title>Laravel blog @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="../css/app.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" integrity="sha384-PmY9l28YgO4JwMKbTvgaS7XNZJ30MK9FAZjjzXtlqyZCqBY6X6bXIkM++IkyinN+" crossorigin="anonymous">

    {{ Html::style('css/styles.css') }}

  <style type="text/css">
         .block{
          background: black;
         border-radius: 5px 5px;
          }
         img{
          border-radius: 5px 5px;
          opacity: 0.65;

         }



  </style>
  </head>